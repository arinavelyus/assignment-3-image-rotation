#include "include/rotate.h"
#include "include/image.h"

struct image *rotate_image(struct image* new_image) {
    if (new_image == NULL) {
        return NULL;
    }
	size_t new_height = get_width(new_image);
	size_t new_width = get_height(new_image);
	struct image* rotate_image = allocImage(new_width, new_height);
	for (size_t i = 0; i < new_height; i++) {
		for (size_t j = 0; j < new_width; j++) {
			struct pixel pixel = get_pixel(new_image, i, new_width - j - 1);
			set_pixel(rotate_image, pixel, j, i);
		}
	} 
	return rotate_image;
}
