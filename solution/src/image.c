#include "include/image.h"

size_t get_width(struct image* image) {
    return image->width;
}

size_t get_height(struct image* image) {
    return image->height;
}

void set_width(struct image* image, size_t width) {
    image->width = width;
}

void set_height(struct image* image, size_t height) {
    image->height = height;
}

struct pixel get_pixel(struct image* image, size_t x, size_t y) {
    return image->pixels[y * image->width + x];
}

void set_pixel(struct image* image, struct pixel pixel, size_t x, size_t y) {
    image->pixels[y * image->width + x] = pixel;
}

struct pixel* get_pixel_arr(struct image* image) {
    return image->pixels;
}

void set_pixel_arr(struct image* image, struct pixel* pixels) {
    image->pixels = pixels;
}

struct pixel* allocPixels(size_t width, size_t height) {
    return malloc(width * height * sizeof (struct pixel));
}

void freePixel(struct pixel* pixels) {
    free(pixels);
}

struct image* allocImage(size_t width, size_t height) {
    struct image* image = malloc(sizeof(struct image));
    image->width = width;
    image->height = height;
    image->pixels = allocPixels(width, height);
    return image;
}

void freeImage(struct image* image) {
    freePixel(image->pixels);
    free(image);
}
