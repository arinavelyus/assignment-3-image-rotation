#include "include/file.h"

enum open_status openFile(FILE** target, const char *name, char *mode) {
    *target = fopen(name, mode);
    if(*target != NULL){
        return OPEN_OK;
    }
    return OPEN_ERROR;
}

enum close_status closeFile(FILE *target) {
    int result = fclose(target);
    if(result == 0){
        return CLOSE_OK; 
    }
    else return CLOSE_ERROR;
}
