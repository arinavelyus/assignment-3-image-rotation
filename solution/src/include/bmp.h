#ifndef BMP
#define BMP
#include "include/image.h"
#include <stdint.h>
#include <stdio.h>

struct bmp_header {
        uint16_t bfType;
        uint32_t  bfileSize;
        uint32_t bfReserved;
        uint32_t bOffBits;
        uint32_t biSize;
        uint32_t biWidth;
        uint32_t  biHeight;
        uint16_t  biPlanes;
        uint16_t biBitCount;
        uint32_t biCompression;
        uint32_t biSizeImage;
        uint32_t biXPelsPerMeter;
        uint32_t biYPelsPerMeter;
        uint32_t biClrUsed;
        uint32_t  biClrImportant;
} __attribute__((packed));

enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_FAIL_NULL
};

enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR
};

uint8_t get_padding(size_t width);
enum read_status read_body( FILE* in, struct image* image );
enum write_status write_body( FILE* out, struct image* image );
enum read_status read_header(FILE* in, struct bmp_header* header);
enum read_status read_pixel_arr(FILE* in, struct image* image, struct bmp_header* header);
void init_header(struct bmp_header* header, struct image* image);
void print_header(struct bmp_header* header);

#endif
