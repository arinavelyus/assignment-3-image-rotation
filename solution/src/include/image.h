#ifndef IMAGE
#define IMAGE
#include <malloc.h>
#include <stdint.h>
#include <stdio.h>

struct image {
    size_t width;
    size_t height;
    struct pixel* pixels;
};

struct pixel {
    uint8_t b, g, r;
};

size_t get_width(struct image* image);
size_t get_height(struct image* image);
void set_width(struct image* image, size_t width);
void set_height(struct image* image, size_t height);
struct pixel get_pixel(struct image* image, size_t x, size_t y);
void set_pixel(struct image* image, struct pixel pixel, size_t x, size_t y);
struct pixel* get_pixel_arr(struct image* image);
void set_pixel_arr(struct image* image, struct pixel* pixels);
struct image* allocImage(size_t width, size_t height);
void freeImage(struct image* image);
struct pixel* allocPixels(size_t width, size_t height);
void freePixel(struct pixel* pixels);
#endif
