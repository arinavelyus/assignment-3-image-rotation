#ifndef FILE
#define FILE

#include <stdio.h>

enum open_status {
	OPEN_OK,
	OPEN_ERROR
};

enum close_status {
	CLOSE_OK,
	CLOSE_ERROR
};


enum open_status openFile(FILE** target, char *name, char *mode);
enum close_status closeFile(FILE *target);

#endif
