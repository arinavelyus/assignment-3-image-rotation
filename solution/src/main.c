
#include "include/bmp.h"
#include "include/image.h"
#include "include/file.h"
#include "include/rotate.h"

#include <stdio.h>

int main( int argc, char** argv ) {
    if (argc < 3) {
        fprintf(stderr, "Usage: ./image-transformer <source-image> <transformed-image>");
        return -1;
    }

    FILE* orig_bmp = openFile(argv[1], "rb");
    FILE* new_bmp = openFile(argv[2], "wb");
    struct image* orig_image = allocImage(0, 0);
    read_body(orig_bmp, orig_image);
    closeFile(orig_bmp);
    struct image* rotated_image = rotate_image(orig_image);
    write_body(new_bmp, rotated_image);
    closeFile(new_bmp);
    printf("DEBUG");
    freeImage(orig_image);
    freeImage(rotated_image);

    return 0;
}
