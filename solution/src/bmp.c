#include "include/bmp.h"

#define WORD_SIZE 4
#define COLOR_DEPTH 24
#define BMP_TYPE 0x4D42
#define BMP_RESERVED 0
#define BMP_COMPRESSION 0
#define BMP_PIXEL_METER 2834
#define BMP_PLANES 1
#define BMP_COLORS 0
#define BMP_HEADER_SIZE 40

uint8_t get_padding(size_t width){//1
    return WORD_SIZE - (width * (COLOR_DEPTH / 8)) % WORD_SIZE;
}

enum read_status read_body(FILE* in, struct image* image) { //1
	if (!in) return READ_FAIL_NULL;
    struct bmp_header header = {0};
    enum read_status header_read_status =  read_header(in, &header);
    if (header_read_status != READ_OK) return header_read_status;
    print_header(&header);
    set_height(image, header.biHeight);
    set_width(image, header.biWidth);
    return read_pixel_arr(in, image, &header);
}

enum write_status write_body(FILE* out, struct image* image) {//1
    if (!out || !image) return WRITE_ERROR;
    struct bmp_header header = {0};
    init_header(&header, image);
    print_header(&header);

    size_t image_width = get_width(image);
    size_t image_height = get_height(image);
    struct pixel* data = get_pixel_arr(image);

    uint8_t padding = get_padding(image_width);
    fwrite(&header, sizeof(struct bmp_header), 1, out);
    fseek(out, header.bOffBits, SEEK_SET);
    uint8_t *padding_trash = calloc(1, padding);

    for (size_t i = 0; i < image_height; i++){
        fwrite(data + (header.biWidth) * i, (header.biWidth) * sizeof(struct pixel), 1, out);
        fwrite(padding_trash, padding, 1, out);
    }
    free(padding_trash);
    return WRITE_OK;
}


enum read_status read_header(FILE* in, struct bmp_header* header) {//1
    rewind(in);
    fread(header, sizeof(struct bmp_header), 1, in);
    return READ_OK;
}

enum read_status read_pixel_arr(FILE* in, struct image *image, struct bmp_header *header) {//1
    size_t img_width = header->biWidth;
    size_t img_height = header->biHeight;
    struct pixel* pixel_arr = allocPixels(img_width, img_height);
    if (pixel_arr == NULL) return READ_FAIL_NULL;
    rewind(in);
    fseek(in, header->bOffBits, SEEK_SET);
    uint8_t padding = get_padding(img_width);
    printf("Image padding is: ", padding);
    for (size_t i = 0; i < img_height; i++) {
        fread(pixel_arr + i * img_width, sizeof (struct pixel) * img_width, 1 , in);
        fseek(in, padding, SEEK_CUR);
    }
    freePixel(get_pixel_arr(image));
    set_pixel_arr(image, pixel_arr);
    return READ_OK;
}

void init_header(struct bmp_header* header, struct image* image) {//1
    size_t image_width = get_width(image);
    size_t image_height = get_height(image);
    uint8_t padding = get_padding(image_width);
    size_t image_size = (sizeof(struct pixel) * image_width + padding) * image_height;

    header->bfType = BMP_TYPE;
    header->biBitCount = COLOR_DEPTH;
    header->biXPelsPerMeter = BMP_PIXEL_METER;
    header->biYPelsPerMeter = BMP_PIXEL_METER;
    header->bfileSize = image_size + BMP_RESERVED + sizeof(struct bmp_header);
    header->bfReserved = BMP_RESERVED;
    header->bOffBits = sizeof(struct bmp_header);
    header->biSize = BMP_HEADER_SIZE;
    header->biWidth = image_width;
    header->biHeight = image_height;
    header->biPlanes = BMP_COMPRESSION;
    header->biCompression = BMP_COMPRESSION;
    header->biSizeImage = image_size;
    header->biClrUsed = BMP_COLORS;
    header->biClrImportant = BMP_COLORS;
}
